import { Injectable } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Dish } from '../models/Dish';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private dishes: Dish[] = [];
  restaurantId: number | undefined;
  constructor(private message: NzMessageService) { }

  addDish(newDish: Dish, id: number | undefined): void{
    if(this.restaurantId && this.restaurantId !== id){
      // A CUSTOMER TÖBB ÉTTEREMBŐL NEM TUD EGYSZERRE RENDELNI, 
      // AZT KÜLÖN RENDELÉSBEN LEHET!
      this.dishes = [];
    }
    this.restaurantId = id;
    this.dishes.push(newDish);
    this.message.success(newDish.name+" hozzáadva a kosárhoz!");
  }

  getDishes(): Dish[]{
    return this.dishes;
  }

  getTotal(): number{
    if(this.dishes.length > 0){
      return Number(this.dishes
      .map(x=>x.price)
      .reduce((accumulator, currentValue)=>Number(accumulator)+Number(currentValue)));
    }
    return 0;
  }

}
