/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Address } from '../models/Address';
import { Observable, of } from 'rxjs';
import { Role } from '../models/Role';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private httpClient: HttpClient, private router: Router) {}

  loggedIn(): boolean {
    return !!localStorage.getItem('jwt');
  }

  login(formValues: any): Observable<any> {
    console.log(formValues);

    return this.httpClient.post<any>(
      'http://localhost:8080/auth/login',
      formValues
    );
    // return of({
    //   access_token: 'xxx.yyy.zzz',
    //   role: Role.OWNER
    // });
  }

  registry(formValues: any, asRole: string): Observable<any> {
    console.log(formValues);
    formValues.role = asRole;
    console.log(this.getRole());
    // return of("ok");
    return this.httpClient.post<any>(
      `http://localhost:8080/auth/register`,
      formValues
    );
  }

  getRole(): Role {
    const roleString: string | null = localStorage.getItem('role');
    if (!roleString) {
      return Role.ANYONE;
    }
    return roleString as Role;
  }

  logout(): void {
    localStorage.clear();
    // ! Nem a legelegánsabb de legalább nem futunk majd hibákba.
    this.router.navigateByUrl('/home').then(() => {
      window.location.reload();
    });
  }

  searchAddress(address: string): Observable<Address[]> {
    // return of(
    //   [
    //     {
    //         id: "EiJTemVnZWQsIEJ1ZGFwZXN0aSBrw7Zyw7p0LCBIdW5nYXJ5Ii4qLAoUChIJHw3VsxyIREcRDY6FYA1WdKASFAoSCUvlzCvih0RHEZARHgwpxAAE",
    //         address: "Szeged, Budapesti körút, Hungary"
    //     },
    //     {
    //         id: "EiBCdWRhcGVzdCwgQnVkYXBlc3RpIMO6dCwgSHVuZ2FyeSIuKiwKFAoSCWW0bOfpxEFHEcFxJh5bNFgnEhQKEgnJz9TRNMNBRxFgER4MKcQABA",
    //         address: "Budapest, Budapesti út, Hungary"
    //     },
    //     {
    //         id: "EiBWZXN6cHLDqW0sIEJ1ZGFwZXN0IMO6dCwgSHVuZ2FyeSIuKiwKFAoSCTuU53x7mmlHETrqR3VQrSEMEhQKEgmRL4wC3ZppRxEQEh4MKcQABA",
    //         address: "Veszprém, Budapest út, Hungary"
    //     },
    //     {
    //         id: "Eh5TemVnZWQsIEJ1ZGFwZXN0aSDDunQsIEh1bmdhcnkiLiosChQKEgmbOHvmfIdERxFPhnQGL5ormxIUChIJS-XMK-KHREcRkBEeDCnEAAQ",
    //         address: "Szeged, Budapesti út, Hungary"
    //     },
    //     {
    //         id: "EiBHeXVsYSwgQnVkYXBlc3Qga8O2csO6dCwgSHVuZ2FyeSIuKiwKFAoSCbt1-jwi1EVHEeEfI1YGS_7EEhQKEgkLKlcukdZFRxFauuFKn7JD5g",
    //         address: "Gyula, Budapest körút, Hungary"
    //     }
    // ]
    // );
    return this.httpClient.get<Address[]>(
      'http://localhost:8080/places/search?place_name=?place_name=' + address
    );
  }
}
