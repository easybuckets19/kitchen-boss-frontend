import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { apiconfig } from '../api-config';

import { Restaurant } from '../models/Restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {
  constructor(private http: HttpClient) {}

  mockRestaurants: Restaurant[] = [
    {
      id: 1,
      name: 'Étterem1',
      description: 'Fasza étterem',
      address: 'CÍM',
      // style: 'menő',
      opensAt: 0,
      closesAt: 24
    },
    {
      id: 2,
      name: 'Étterem2',
      description: 'Legfaszább étterem',
      address: 'CÍM',
      // style: 'menő',
      opensAt: 0,
      closesAt: 24
    },
    {
      id: 3,
      name: 'Cucc',
      description: 'Legfaszább étterem',
      address: 'CÍM',
      // style: 'menő',
      opensAt: 0,
      closesAt: 24
    },
    {
      id: 3,
      name: 'Nem tom',
      description: 'Legfaszább étterem',
      address: 'CÍM',
      // style: 'menő',
      opensAt: 0,
      closesAt: 24
    },
    {
      id: 3,
      name: 'Valami',
      description: 'Legfaszább étterem',
      address: 'CÍM',
      // style: 'menő',
      opensAt: 0,
      closesAt: 24
    }
  ];

  getRestaurants(): Observable<Restaurant[]> {
    return this.http.get<Restaurant[]>(apiconfig.orderApiUrl+'/restaurants');
    // return of(this.mockRestaurants);
  }

  registryRestaurant(restaurantRegData: any): Observable<any> {
    console.log('registry new restaurant: ', restaurantRegData);
    return this.http.post<any>(
      'http://localhost:8080/restaurants',
      restaurantRegData
    );
    // return of('sikeres étterem regisztráció');
  }

  getOwnerRestaurants(): Observable<Restaurant[]>{
    // return of([this.mockRestaurants[0], this.mockRestaurants[1]]);
    return this.http.get<Restaurant[]>(apiconfig.apiUrl+"/restaurants/by-owner");
  }

}
