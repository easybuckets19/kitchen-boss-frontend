import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { apiconfig } from '../api-config';
import { Observable, Subject } from 'rxjs';
import { Order } from '../models/Order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private readonly _newOrderSub = new Subject<Order>();
  private readonly _newJobSub = new Subject<Order>();
  private readonly _newStatusSub = new Subject<any>();

  constructor(private http: HttpClient) {
    this.initSSE();
  }

  private initSSE(): void {
    const AUTH_PARAMS = {
      access_token: localStorage.getItem('jwt')!.toString()
    };
    const sse = new EventSource(
      apiconfig.apiUrl +
        '/sse-connect?access_token='.concat(AUTH_PARAMS.access_token)
    );

    sse.addEventListener('new_order', (event: Event) => {
      console.log('new order event');
      const msg = event as MessageEvent;
      const order = JSON.parse(msg.data) as Order;
      this._newOrderSub.next(order);
    });

    sse.addEventListener('new_job', (event: Event) => {
      const msg = event as MessageEvent;
      const order = JSON.parse(msg.data) as Order;
      this._newJobSub.next(order);
    });

    sse.addEventListener('new_status', (event: Event) => {
      const msg = event as MessageEvent;
      const order = JSON.parse(msg.data) as Order;
      this._newStatusSub.next(order);
    });
  }

  getNewOrderSubject(): Subject<Order> {
    return this._newOrderSub;
  }

  getOrderStatusSubject(): Subject<any> {
    return this._newStatusSub;
  }

  getNewJobSubject(): Subject<Order> {
    return this._newJobSub;
  }

  getCurrentOrders(restaurantId: number): Observable<Order[]> {
    return this.http.get<Order[]>(
      apiconfig.orderApiUrl + '/restaurants/' + restaurantId + '/orders'
    );
  }

  assignOrderToDelivery(deliveryId: number, order: Order): Observable<any> {
    return this.http.post<any>(
      apiconfig.orderApiUrl + '/delivery/new-job/' + deliveryId,
      {
        orderId: order.id,
        orderStatus: order.orderStatus
      }
    );
  }
}
