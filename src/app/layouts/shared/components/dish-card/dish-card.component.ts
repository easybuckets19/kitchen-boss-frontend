import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Dish } from 'src/app/models/Dish';
import { Role } from 'src/app/models/Role';
import { AuthService } from 'src/app/services/auth.service';
import { CartService } from 'src/app/services/cart.service';
import { DishService } from 'src/app/services/dish.service';

@Component({
  selector: 'app-dish-card',
  templateUrl: './dish-card.component.html',
  styleUrls: ['./dish-card.component.less']
})
export class DishCardComponent implements OnInit{
  @Input('dish') dish: Dish = {};
  @Input('restaurantId') restaurantId: number | undefined;

  modifyDishVisible: boolean = false;
  priceModifySuccessString: string = "";
  priceModifyErrorString: string = "";

  constructor(private cartService: CartService, public authService: AuthService,
    private dishService: DishService){}
  Role = Role;
  ngOnInit(){
  }

  toCart(): void{
    console.log("dish: "+this.dish.id + ", put to cart, for restaurant: "+this.restaurantId);
    this.cartService.addDish(this.dish, this.restaurantId);
  }

  modifyDishPrice(): void{
    console.log("dish price modify: "+this.dish.id +"-> new price: "+this.dish.price);
    this.dishService.modifyDishPrice(
      Number(this.restaurantId), 
      Number(this.dish.id), 
      Number(this.dish.price))
        .subscribe((res: any)=>{
          this.priceModifySuccessString = "Sikeres ármódosítás!";
          this.priceModifyErrorString = "";
        },
        (err: HttpErrorResponse)=>{
          this.priceModifySuccessString = "";
          this.priceModifyErrorString = "Sikertelen ármódosítás!";
          this.priceModifyErrorString += " "+err.message;
        });
  }

  change(value: boolean): void {
    //console.log(value);
  }

  clickMe(): void {
    this.modifyDishVisible = false;
  }

}
