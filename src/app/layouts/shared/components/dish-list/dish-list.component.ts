/* eslint-disable prettier/prettier */
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Dish } from 'src/app/models/Dish';
import { DishService } from 'src/app/services/dish.service';

@Component({
  selector: 'app-dish-list',
  templateUrl: './dish-list.component.html',
  styleUrls: ['./dish-list.component.less']
})
export class DishListComponent implements OnInit {
  dishes: Dish[] = [];
  dishOptions: string[] = [];
  searchedDish = '';
  showAll = true;
  dishFiltered: Dish[] = [];
  dishCategories: string[] = [];

  restaurantId: number | undefined;
  constructor(private dishService: DishService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.restaurantId = Number(this.route.snapshot.paramMap.get("restaurantId"));
    console.log('This menu is from restaurantId: '+this.restaurantId);
    this.dishService.getRestaurantMenu(this.restaurantId)
      .subscribe((menu: Dish[])=>{
        console.log(menu);
        this.dishes = menu;
        this.dishes.forEach((dish: Dish)=>{
          if(!this.dishCategories.includes(String(dish.category))){
            this.dishCategories.push(String(dish.category));
          }
        });
      });

    // this.dishService.getDishCategories()
    //   .subscribe((categories: string[])=>{
    //     this.dishCategories = categories;
    //   });
  }

  onChange(value: string): void {
    this.showAll = true;
    this.dishFiltered = this.dishes.filter((option: Dish) => {
      return option.name!.toLowerCase().indexOf(value.toLowerCase()) !== -1;
    });
    this.dishOptions = this.dishFiltered.map((a) => a.name!);
  }

  onSearch(): void {
    if (this.searchedDish !== '') {
      this.showAll = false;
    }
  }
}
