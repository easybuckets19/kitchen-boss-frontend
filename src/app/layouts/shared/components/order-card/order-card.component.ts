import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Delivery } from 'src/app/models/Delivery';
import { Order } from 'src/app/models/Order';
import { OrderStatus } from 'src/app/models/OrderStatus';
import { DeliveryService } from 'src/app/services/delivery.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.less']
})
export class OrderCardComponent implements OnInit {
  @Input('order') order!: Order;
  @Input('deliveries') deliveries!: Delivery[];

  disabledAssign: boolean = false;

  constructor(
    private orderService: OrderService,
    private message: NzMessageService,
    private deliveryService: DeliveryService) { }

  ngOnInit(): void {
    console.log(this.order.type === 'TYPE_DELIVERY');
  }

  // printOrderLines(): string{
  //   let result = "";
  //   this.order.orderLine.forEach(element => {
  //     result += "food Id: "+element.foodId+" qty: "+element.qty+", ";
  //   });
  //   return result;
  // }

  assignToDelivery(deliveryId: number): void{
    this.orderService.assignOrderToDelivery(deliveryId, this.order)
      .subscribe(
        (res: any)=>{
          this.message.success("Sikeresen hozzárendelve!");
          this.disabledAssign=true;
        },
        (err: HttpErrorResponse)=>{
          this.message.error("Hiba a hozzárendelés során!"+err.message);
        });
  }

  jobDone(): void{
    let statusChangeObj = { orderId: this.order.id,  orderStatus: 'STATUS_DONE' };
    this.deliveryService.changeJobStatus(statusChangeObj)
      .subscribe(
        (res: any)=>{
          this.message.success("Sikeres mentés!");
          this.order.orderStatus = 'STATUS_DONE';
        },
        (err: HttpErrorResponse)=>{
          this.message.error("Sikertelen mentés: "+err.message);
        }
      );


  }

}
