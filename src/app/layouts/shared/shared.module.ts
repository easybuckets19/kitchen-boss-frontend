import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { IconsProviderModule } from './icons-provider.module';
import { DishCardComponent } from './components/dish-card/dish-card.component';
import { DishListComponent } from './components/dish-list/dish-list.component';
import { DishCategoryFilterPipe } from '../../pipes/dish-category-filter.pipe';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { OrderCardComponent } from './components/order-card/order-card.component';

@NgModule({
  declarations: [
    DishCardComponent,
    DishListComponent,
    DishCategoryFilterPipe,
    OrderCardComponent
  ],
  imports: [
    // - Angular
    CommonModule,
    FormsModule,
    HttpClientModule,
    // - NG-ZORRO
    NzSelectModule,
    NzInputModule,
    NzInputNumberModule,
    NzButtonModule,
    NzCardModule,
    NzAutocompleteModule,
    IconsProviderModule,
    NzPopoverModule,
    NzRadioModule
  ],
  exports: [
    // - Angular
    CommonModule,
    FormsModule,
    HttpClientModule,
    // - NG-ZORRO
    NzSelectModule,
    NzInputModule,
    NzInputNumberModule,
    NzButtonModule,
    NzCardModule,
    NzAutocompleteModule,
    IconsProviderModule,
    NzPopoverModule,
    NzRadioModule,
    // - Shared components
    DishCardComponent,
    DishListComponent,
    DishCategoryFilterPipe,
    NzPopoverModule,
    OrderCardComponent
  ]
})
export class SharedModule {}
