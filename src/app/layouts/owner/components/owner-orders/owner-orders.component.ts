import { Component, OnInit } from '@angular/core';
import { Delivery } from 'src/app/models/Delivery';
import { Order } from 'src/app/models/Order';
import { Restaurant } from 'src/app/models/Restaurant';
import { SocketMessage } from 'src/app/models/SocketMessage';
import { DeliveryService } from 'src/app/services/delivery.service';
import { OrderService } from 'src/app/services/order.service';
import { RestaurantsService } from 'src/app/services/restaurants.service';

@Component({
  selector: 'app-owner-orders',
  templateUrl: './owner-orders.component.html',
  styleUrls: ['./owner-orders.component.less']
})
export class OwnerOrdersComponent implements OnInit {

  orders: Order[] = [];
  resturantId: number = 0;
  restaurants: Restaurant[] = [];
  deliveries: Delivery[] = [];
  String = String;
  Number = Number;
  constructor(
    private orderService: OrderService,
    private restaurantService: RestaurantsService,
    private deliveryService: DeliveryService) { }

  ngOnInit(): void {
    this.restaurantService.getOwnerRestaurants()
      .subscribe((restaurants: Restaurant[])=>{
        this.restaurants = restaurants;
        this.resturantId = Number(this.restaurants[0].id);
        console.log(restaurants);

        this.updateCurrentOrders();
      });

    this.orderService.getNewOrderSubject()
      .subscribe(msg=>{
        console.log(msg);
        this.orders.push(msg);
      });

    this.orderService.getOrderStatusSubject()
      .subscribe(msg=>{
        this.orders.find((order: Order)=>order.id === msg.id)!.orderStatus = msg.orderStatus;
      });

      //deliveries
      this.deliveryService.getAllDeliveryWorkers()
      .subscribe((deliveries: Delivery[])=>{
        this.deliveries = deliveries;
        console.log("deliveries", this.deliveries);
      });
  }

  // printOrderLines(order: Order): string{
  //   let result = "";
  //   order.orderLine.forEach(element => {
  //     result += "food Id: "+element.foodId+" qty: "+element.qty+", ";
  //   });
  //   return result;
  // }

  updateCurrentOrders(){
    this.orderService.getCurrentOrders(this.resturantId)
      .subscribe((currentOrders: Order[])=>{
        this.orders = currentOrders;
      });
  }

}
