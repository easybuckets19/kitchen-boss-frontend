import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-menu-category',
  templateUrl: './add-menu-category.component.html',
  styleUrls: ['./add-menu-category.component.less']
})
export class AddMenuCategoryComponent implements OnInit {
  newMenuCategory = '';

  ngOnInit(): void {}
}
