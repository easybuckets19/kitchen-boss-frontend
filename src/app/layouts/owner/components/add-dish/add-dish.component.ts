import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Allergene } from 'src/app/models/Allergene';
import { Dish } from 'src/app/models/Dish';
import { DishCategory } from 'src/app/models/DishCategory';
import { Restaurant } from 'src/app/models/Restaurant';
import { DishService } from 'src/app/services/dish.service';
import { RestaurantsService } from 'src/app/services/restaurants.service';

@Component({
  selector: 'app-add-dish',
  templateUrl: './add-dish.component.html',
  styleUrls: ['./add-dish.component.less']
})
export class AddDishComponent implements OnInit {
  selectedCategory: string = '';
  name = '';
  price = 0;
  listOfAllergens: string[] = [];
  listOfSelectedAllergens = [];
  listOfCategories: string[] = [];
  listOfRestaurants: Restaurant[] = [];
  selectedRestaurantId: number = 0;
  description: string = '';
  
  successString = "";
  errorString = "";

  String = String;

  constructor(private dishService: DishService, private restaurantService: RestaurantsService){}

  ngOnInit(): void {
    this.restaurantService.getOwnerRestaurants()
      .subscribe((restaurants: Restaurant[])=>{
        this.listOfRestaurants = restaurants;
      });
    this.dishService.getAllergenes()
      .subscribe((allergenes: string[])=>this.listOfAllergens = allergenes);
    this.dishService.getDishCategories()
      .subscribe((categories: string[])=>{
        this.listOfCategories = categories;
      });
  }

  onSubmit(): void{
    let newDish: Dish = {
      name: this.name,
      allergenes: this.listOfSelectedAllergens,
      description: this.description,
      price: this.price,
      category: this.selectedCategory
    };
    console.log("new dish: ",newDish, ", for restaurant: "+this.selectedRestaurantId);
    this.dishService.registryDish(this.selectedRestaurantId, newDish)
      .subscribe((res: any)=>{
        console.log(res);
        this.errorString = "";
        this.successString ="Sikeres ételrögzítés!";
      },
      (error: HttpErrorResponse)=>{
        this.successString = "";
        this.errorString = "Hiba: "+error.message;
      });
  }
}
