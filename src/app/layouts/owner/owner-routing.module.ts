import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddDishComponent } from './components/add-dish/add-dish.component';
import { DishListComponent } from '../shared/components/dish-list/dish-list.component';
import { OwnerOrdersComponent } from './components/owner-orders/owner-orders.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'add-dish',
    pathMatch: 'full'
  },
  {
    path: 'add-dish',
    component: AddDishComponent
  },
  {
    path: 'dish-list',
    component: DishListComponent
  },
  {
    path: 'orders',
    component: OwnerOrdersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnerRoutingModule {}
