import { NgModule } from '@angular/core';

import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';

import { SharedModule } from '../shared/shared.module';
import { OwnerRoutingModule } from './owner-routing.module';

import { AddDishComponent } from './components/add-dish/add-dish.component';
import { AddMenuCategoryComponent } from './components/add-menu-category/add-menu-category.component';
import { OwnerOrdersComponent } from './components/owner-orders/owner-orders.component';
import { OrderCardComponent } from '../shared/components/order-card/order-card.component';

@NgModule({
  declarations: [
    AddDishComponent, 
    AddMenuCategoryComponent, 
    OwnerOrdersComponent
  ],
  imports: [
    // - NG-ZORRO
    NzSelectModule,
    NzInputModule,
    NzInputNumberModule,
    NzButtonModule,
    NzCardModule,
    NzAutocompleteModule,
    // - Shared
    SharedModule,
    // - Routing
    OwnerRoutingModule
  ]
})
export class OwnerModule {}
