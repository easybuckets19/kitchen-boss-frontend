import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryRoutingModule } from './delivery-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DeliveryJobsComponent } from './components/delivery-jobs/delivery-jobs.component';
import { JobCardComponent } from './components/job-card/job-card.component';



@NgModule({
  declarations: [DeliveryJobsComponent, JobCardComponent],
  imports: [
    SharedModule, DeliveryRoutingModule
  ]
})
export class DeliveryModule { }
