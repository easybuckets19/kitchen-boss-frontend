import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Order } from 'src/app/models/Order';
import { OrderStatus } from 'src/app/models/OrderStatus';
import { DeliveryService } from 'src/app/services/delivery.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-job-card',
  templateUrl: './job-card.component.html',
  styleUrls: ['./job-card.component.less']
})
export class JobCardComponent implements OnInit {
  @Input('order') order!: Order;
  disabledAssign: boolean = false;

  constructor(
    private orderService: OrderService,
    private message: NzMessageService,
    private deliveryService: DeliveryService
  ) {}
  ngOnInit(): void {}

  printOrderLines(): string {
    let result = '';
    this.order.orderLines.forEach((element) => {
      result +=
        'Név: ' + element.food + ' mennyiség: ' + element.quantity + ', ';
    });
    return result;
  }

  acceptJob(): void {
    console.log('job accepted');
    // this.message.success("Munka elvállalva!");
    let orderStatus: OrderStatus = 'STATUS_ACCEPTED';
    let orderStatusObject = {
      orderId: this.order.id,
      orderStatus: orderStatus
    };
    this.deliveryService.changeJobStatus(orderStatusObject).subscribe(
      (res: any) => {
        this.message.success('Munka sikeresen elfogava!');
        this.order.orderStatus = orderStatus;
      },
      (err: HttpErrorResponse) => {
        this.message.error('Sikeretlen művelet!' + err.message);
      }
    );
  }

  rejectJob(): void {
    console.log('job rejected');
    let orderStatus: OrderStatus = 'STATUS_REJECTED';
    let orderStatusObject = {
      orderId: this.order.id,
      orderStatus: orderStatus
    };
    this.deliveryService.changeJobStatus(orderStatusObject).subscribe(
      (res: any) => {
        this.message.success('Munka visszautasítva!');
        this.order.orderStatus = orderStatus;
      },
      (err: HttpErrorResponse) => {
        this.message.error('Sikeretlen művelet!' + err.message);
      }
    );
  }

  jobDone(): void {
    console.log('job done');
    let orderStatus: OrderStatus = 'STATUS_DONE';
    let orderStatusObject = {
      orderId: this.order.id,
      orderStatus: orderStatus
    };
    this.deliveryService.changeJobStatus(orderStatusObject).subscribe(
      (res: any) => {
        this.message.success('Munka sikeresen elvégezve!');
        this.order.orderStatus = orderStatus;
      },
      (err: HttpErrorResponse) => {
        this.message.error('Sikeretlen művelet!' + err.message);
      }
    );
  }
}
