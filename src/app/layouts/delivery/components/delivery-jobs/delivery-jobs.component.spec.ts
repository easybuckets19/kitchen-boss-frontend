import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryJobsComponent } from './delivery-jobs.component';

describe('DeliveryJobsComponent', () => {
  let component: DeliveryJobsComponent;
  let fixture: ComponentFixture<DeliveryJobsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryJobsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
