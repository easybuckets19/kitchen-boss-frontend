import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/models/Order';
import { DeliveryService } from 'src/app/services/delivery.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-delivery-jobs',
  templateUrl: './delivery-jobs.component.html',
  styleUrls: ['./delivery-jobs.component.less']
})
export class DeliveryJobsComponent implements OnInit {

  currentOrders: Order[] = [];
  constructor(private deliveryService: DeliveryService,
    private orderService: OrderService) { }

  ngOnInit(): void {
    this.deliveryService.getAssignedJobs()
      .subscribe((jobs: Order[])=>{
        this.currentOrders = jobs;
      });

      this.orderService.getNewJobSubject()
        .subscribe((newJob: Order)=>{
          this.currentOrders.push(newJob);
        });

  }

}
