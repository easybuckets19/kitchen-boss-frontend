import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliveryJobsComponent } from './components/delivery-jobs/delivery-jobs.component';

const routes: Routes = [
//   {
//     path: '',
//     component: CustomerTestComponent
//   },
  {
    path: 'jobs',
    component: DeliveryJobsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryRoutingModule {}
