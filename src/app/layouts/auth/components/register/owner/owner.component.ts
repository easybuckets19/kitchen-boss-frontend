import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Role } from 'src/app/models/Role';
import { AuthService } from 'src/app/services/auth.service';
import { RestaurantsService } from 'src/app/services/restaurants.service';

@Component({
  selector: 'app-owner',
  templateUrl: './owner.component.html',
  styleUrls: ['./owner.component.less']
})
export class OwnerComponent implements OnInit {
  ownerForm: FormGroup = new FormGroup({
    shared: new FormControl([]),
    restaurantName: new FormControl(''),
    restaurantDescription: new FormControl(''),
    restaurantLocation: new FormControl(''),
    restaurantStyle: new FormControl(''),
    restaurantOpeningHours: new FormControl(0),
    closesAt: new FormControl(24)
  });

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private restaurantService: RestaurantsService
  ) {}

  errorString = "";
  successString = "";

  ngOnInit(): void {
    this.ownerForm = this.formBuilder.group({
      shared: [],
      restaurantName: '',
      restaurantDescription: '',
      restaurantLocation: '',
      restaurantStyle: '',
      opensAt: 0,
      closesAt: 24,
      restaurantZipCode: '',
      restaurantCity: '',
      restaurantStreet: '',
      restaurantHouseNo: ''
    });
  }
  formSubmit(): void {
    // Regsitry owner
    // console.log(this.ownerForm.value);

    const regObject = this.ownerForm.value.shared;
    const restaurantRegData = {
      name: this.ownerForm.value.restaurantName,
      description: this.ownerForm.value.restaurantDescription,
      placeId: regObject.placeId,
      styles: [this.ownerForm.value.restaurantStyle],
      opensAt: this.ownerForm.value.opensAt,
      closesAt: this.ownerForm.value.closesAt
    };
    console.log(restaurantRegData);
    regObject.role = 'ROLE_OWNER';
    this.authService.registry(regObject, Role.OWNER).subscribe((res: any) => {
      console.log('response from server: ', res);
      this.successString = "sikeres tulajdonos regiszrtáció! "
      // Registry restaurant
      this.restaurantService
        .registryRestaurant(restaurantRegData)
        .subscribe((res: any) => {
          console.log('response from server: ', res);
          this.errorString = "";
          this.successString += "sikeres étterem regisztáció"!;
        },
        (error: HttpErrorResponse)=>{
          this.errorString += "Hiba az étterem regnél";
        });
    },
    (error: HttpErrorResponse)=>{
      this.errorString = "Hiba a tulaj regnél";
    });
  }
}
