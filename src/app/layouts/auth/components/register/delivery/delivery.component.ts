import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Role } from 'src/app/models/Role';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.less']
})
export class DeliveryComponent implements OnInit {
  deliveryForm: FormGroup = new FormGroup({
    shared: new FormControl([]),
    radius: new FormControl(0)
  });

  errorString = '';
  successString = '';

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.deliveryForm = this.formBuilder.group({
      shared: [],
      radius: 0
    });
  }
  formSubmit(): void {
    console.log(this.deliveryForm.value);
    const regObject = this.deliveryForm.value.shared;

    regObject.radius = this.deliveryForm.value.radius;
    this.authService
      .registry(regObject, Role.DELIVERY)
      .subscribe((res: any) => {
        console.log('response from server: ', res);
        this.errorString = "";
        this.successString = "Sikeres regisztárció!";
      },
      (error: HttpErrorResponse)=>{
        this.successString = '';
        this.errorString = 'Valami hiba történt';
      });
  }
}
