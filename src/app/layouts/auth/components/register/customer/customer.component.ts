import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Role } from 'src/app/models/Role';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.less']
})
export class CustomerComponent implements OnInit {
  customerForm: FormGroup = new FormGroup({
    shared: new FormControl([])
  });

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.customerForm = this.formBuilder.group({
      shared: []
    });
  }
  formSubmit(): void {
    // console.log(this.customerForm.value);
    const regObject = this.customerForm.value.shared;
    this.authService
      .registry(regObject, Role.CUSTOMER)
      .subscribe((res: any) => {
        console.log('response from server: ', res);
      });
  }
}
