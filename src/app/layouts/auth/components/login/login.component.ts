/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    remember: new FormControl(false)
  });

  errorString:string | undefined;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: [
        null,
        [Validators.required, Validators.minLength(6), Validators.maxLength(12)]
      ],
      password: [
        null,
        [Validators.required, Validators.minLength(8), Validators.maxLength(16)]
      ],
      remember: [false, Validators.required]
    });
  }

  submitForm(): void {
    for (const i in this.loginForm.controls) {
      this.loginForm.controls[i].markAsDirty();
      this.loginForm.controls[i].updateValueAndValidity();
    }
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value)
        .subscribe((res: any) => {
          this.errorString = undefined;
          // TODO: ha rossz atokat adott meg valaki akkor azt lekezelni
          console.log('response from server: ', res);
          localStorage.setItem('jwt', res.access_token);
          localStorage.setItem('role', res.role);
          this.router.navigate(['/home']);
        },
        (error: any)=>{
          // hibás adatok
          this.errorString = 'Hibás bejeltkezési adatok!';
        });
    }
  }
}
