import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzCollapseModule } from 'ng-zorro-antd/collapse';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';

import { SharedModule } from '../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { SharedFormComponent } from './components/register/shared-form/shared-form.component';
import { CustomerComponent } from './components/register/customer/customer.component';
import { OwnerComponent } from './components/register/owner/owner.component';
import { DeliveryComponent } from './components/register/delivery/delivery.component';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';

@NgModule({
  declarations: [
    AuthComponent,
    LoginComponent,
    RegisterComponent,
    SharedFormComponent,
    CustomerComponent,
    OwnerComponent,
    DeliveryComponent
  ],
  imports: [
    // - Angular
    ReactiveFormsModule,
    // - NG-ZORRO
    NzCardModule,
    NzTypographyModule,
    NzFormModule,
    NzInputModule,
    NzCheckboxModule,
    NzButtonModule,
    NzTabsModule,
    NzSliderModule,
    NzCollapseModule,
    NzInputNumberModule,
    NzAutocompleteModule,
    // - Shared
    SharedModule,
    // - Routing
    AuthRoutingModule
  ]
})
export class AuthModule {}
