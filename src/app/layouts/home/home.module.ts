import { NgModule } from '@angular/core';

import { NzInputModule } from 'ng-zorro-antd/input';
import { NzAutocompleteModule } from 'ng-zorro-antd/auto-complete';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzButtonModule } from 'ng-zorro-antd/button';

import { SharedModule } from '../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';
import { RestaurantCardComponent } from './components/restaurant-card/restaurant-card.component';

@NgModule({
  declarations: [HomeComponent, RestaurantCardComponent],
  imports: [
    // - NG-ZORRO
    NzInputModule,
    NzAutocompleteModule,
    NzCardModule,
    NzButtonModule,
    // - Shared
    SharedModule,
    // - Routing
    HomeRoutingModule
  ]
})
export class HomeModule {}
