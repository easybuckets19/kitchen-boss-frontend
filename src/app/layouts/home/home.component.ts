import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/models/Role';
import { AuthService } from 'src/app/services/auth.service';

import { Restaurant } from '../../models/Restaurant';
import { RestaurantsService } from '../../services/restaurants.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {
  restaurants: Restaurant[] = [];
  restaurantOptions: string[] = [];
  searchedRestaurant = '';
  showAll = true;
  restaurantFiltered: Restaurant[] = [];

  constructor(private restaurantService: RestaurantsService, private authService: AuthService) {}

  ngOnInit(): void {
    if(this.authService.getRole() === Role.OWNER){
      // AZ OWNER CSAK A SAJÁT ÉTTERMEIT AKARJA MEGNÉZNI
      this.restaurantService.getOwnerRestaurants()
        .subscribe((restaurants: Restaurant[])=>{
          this.restaurants = restaurants;
        });
    }else{
      // HA NEM OWNER AKKOR MINDEN ÉTTERMET
      this.restaurantService
      .getRestaurants()
      .subscribe((restaurants: Restaurant[]) => {
        this.restaurants = restaurants;
      });
    }
  }

  onChange(value: string): void {
    this.showAll = true;
    this.restaurantFiltered = this.restaurants.filter((option: Restaurant) => {
      return option.name!.toLowerCase().indexOf(value.toLowerCase()) !== -1;
    });
    this.restaurantOptions = this.restaurantFiltered.map((a) => a.name!);
  }

  onSearch(): void {
    if (this.searchedRestaurant !== '') {
      this.showAll = false;
    }
  }
}
