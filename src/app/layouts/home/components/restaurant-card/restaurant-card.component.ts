import { state } from '@angular/animations';
import { Component, Input } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

import { Restaurant } from '../../../../models/Restaurant';

@Component({
  selector: 'app-restaurant-card',
  templateUrl: './restaurant-card.component.html',
  styleUrls: ['./restaurant-card.component.less']
})
export class RestaurantCardComponent {
  @Input('restaurant') restaurant: Restaurant = {};

  constructor(private router: Router){}

showMenu(): void{
  //console.log("show "+this.restaurant.id+"'s menu");
  this.router.navigate(["/home/menu", this.restaurant.id]);
}


}
