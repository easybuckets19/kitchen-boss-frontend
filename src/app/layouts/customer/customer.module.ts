import { NgModule } from '@angular/core';
import { CustomerTestComponent } from './components/customer-test/customer-test.component';
import { CustomerRoutingModule } from './customer-routing.module';
import { SharedModule } from '../shared/shared.module';
import { OrderPageComponent } from './components/order-page/order-page.component';

@NgModule({
  declarations: [CustomerTestComponent, OrderPageComponent],
  imports: [SharedModule, CustomerRoutingModule]
})
export class CustomerModule {}
