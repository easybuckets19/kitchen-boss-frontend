import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CustomerOrder } from 'src/app/models/CustomerOrder';
import { CustomerOrderLine } from 'src/app/models/CustomerOrderLine';
import { Dish } from 'src/app/models/Dish';
import { OrderLine } from 'src/app/models/OrderLine';
import { OrderType } from 'src/app/models/OrderType';
import { CartService } from 'src/app/services/cart.service';
import { DishService } from 'src/app/services/dish.service';

@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.less']
})
export class OrderPageComponent implements OnInit {
  orderLines: CustomerOrderLine[] = [];
  dishes: Dish[] = [];
  orderSuccessString: string = "";
  orderErrorString: string = "";

  selectedOrderType: OrderType = 'TYPE_DELIVERY';
  orderType: any[] = [
    {type: "TYPE_DELIVERY", label: "Házhoz szállítás"},
    {type: "TYPE_RESTAURANT", label: "Étteremben átvétel"},
  ];

  constructor(public cartService: CartService, private dishService: DishService) { }

  ngOnInit(): void {
    console.log(this.cartService.getDishes());
    console.log(this.cartService.restaurantId);
    this.dishes = this.cartService.getDishes();
    this.cartService.getDishes().forEach((dish: Dish)=>{
      if(this.orderLines
        .map((orderLine: CustomerOrderLine)=>orderLine.foodId)
        .includes(Number(dish.id)))
      {
        const orderLine = this.orderLines
          .find((orderLine: CustomerOrderLine)=>Number(dish.id) === orderLine.foodId);
        if(orderLine){
          orderLine.qty = Number(orderLine?.qty) + 1;
        }
      }else{
        this.orderLines.push({foodId: Number(dish.id), qty: 1});
      }
    });
  }
  getFood(id: number): Dish{
    return this.dishes.find((dish: Dish)=>dish.id === id)!;
  }

  onOrder(): void{
    console.log(this.cartService.restaurantId);
    console.log(this.orderLines);
    const order: CustomerOrder = {
      // restaurantId: Number(this.cartService.restaurantId),
      orderLine: this.orderLines,
      type: this.selectedOrderType
    }
    this.dishService.order(Number(this.cartService.restaurantId), order)
      .subscribe((res: any)=>{
        console.log("sikeres rendelés!");
        this.orderSuccessString = "A rendelést sikeresen rögzítettük!";
        this.orderErrorString = "";
      },
      (error: HttpErrorResponse)=>{
        console.log("sikertelen rendelés!");
        this.orderSuccessString = "";
        this.orderErrorString="Hiba történt a rendelés során! "+error.message
      });
  }
}
