import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DishListComponent } from '../shared/components/dish-list/dish-list.component';
import { CustomerTestComponent } from './components/customer-test/customer-test.component';
import { OrderPageComponent } from './components/order-page/order-page.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerTestComponent
  },
  {
    path: 'order',
    component: OrderPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule {}
