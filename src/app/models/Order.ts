import { OrderLine } from "./OrderLine";
import { OrderStatus } from "./OrderStatus";
import { OrderType } from "./OrderType";

export interface Order{
    id:           number;
    address:      string;
    restaurant:   string;
    orderedBy:    string;
    time:         Date;
    type:         OrderType;
    orderStatus:  OrderStatus;
    orderLines:   OrderLine[];
}
