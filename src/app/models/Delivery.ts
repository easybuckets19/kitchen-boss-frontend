export interface Delivery{
    id: number,
    firstName: string,
    lastName: string
}