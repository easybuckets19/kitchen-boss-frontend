export interface OrderLine{
  id:         number;
  food:       string;
  price:      number;
  quantity:   number;
  lineTotal:  number;
}
