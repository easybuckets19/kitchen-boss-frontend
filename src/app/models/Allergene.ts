export interface Allergene{
    id: number,
    name: string,
}