import { Pipe, PipeTransform } from '@angular/core';
import { Dish } from '../models/Dish';

@Pipe({
  name: 'dishCategoryFilter',
  pure: false
})
export class DishCategoryFilterPipe implements PipeTransform {

  transform(items: Dish[], filter: any): any {
    if (!items || !filter) {
        return items;
    }
    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return items.filter((dish: Dish)=>{
      return dish.category === filter.category;
    });
  }

}
